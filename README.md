# Rick and Morty - GraphQL

![R&M](resources/image.png 'R&M')

This is a sample server with a GraphQL API that returns Rick and Morty
characters.

## Project Requirements

- Node LTS
- Yarn
- Docker (Not mandatory)

## About the project

This project exposes a GraphQL API with Rick and Morty characters and properties of them. It is implemented in Typescript and using [Apollo Server](https://www.apollographql.com/docs/apollo-server/).

### Project Decisions

- I decided to have a local database (in a file) for the sake of simplicity but with the possibility to make queries.
- I choose Typescript with Node, because I thought it was simpler to implement a GraphQL API with this language and framework (I've never implemented one before).
- I created a script to pull the characters from the Rick and Morty API, to populate the database, and pushed it to the repo. You can run it with the following command:

```bash
yarn script:fetch
```

- To allow building the project in easier way I've added a Dockerfile to it
- Just to share how we could add a CI/CD pipeline to it, I've created a `.gitlab-ci.yml` with a simple pipeline as demonstration

## Build and Run

### Running the project (Docker)

To build the project using Docker run the following command:

```bash
docker build -t rick-and-morty:latest .
```

To run the project run:

```bash
docker run -p 4000:4000 rick-and-morty
```

### Development environment

If you want to run the project locally (without building it) you need to
run the following:

- Install packages

```bash
yarn
```

- Run the server

```bash
yarn start
```

- Run the server in watch mode (if you are working on it)

```bash
yarn start:watch
```

### Production environment

If you want to build the project run:

```bash
yarn build
```

It will output the transpiled files to the `dist` folder.

## Calling the project API

Project runs in `http://localhost:4000/`. If you are running in local
environment you should see the sandbox and can start querying. Otherwise
you can enter [here](https://studio.apollographql.com/sandbox/explorer)
and point the sandbox to that address.

Some examples of queries could be found in `examples` folder.
