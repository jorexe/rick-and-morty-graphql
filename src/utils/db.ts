import Datastore from '@seald-io/nedb';

const db: Datastore.default =
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  new Datastore({ filename: 'characters.db', autoload: true });

await db.ensureIndexAsync({ fieldName: 'id', unique: true });

export async function clearDb() {
  await db.removeAsync({}, { multi: true });
}

export default db;
