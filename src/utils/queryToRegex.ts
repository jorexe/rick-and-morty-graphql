function stringToRegex(str: string): RegExp {
  return new RegExp(str, 'i');
}

/**
 * Function to transform values of query to regex
 *
 * We need this function because our database uses equality by default
 *
 * @param obj to transform
 * @returns record with regex values
 */
export default function (obj: Record<string, string>): Record<string, RegExp> {
  return Object.fromEntries(
    Object.entries(obj).map(([k, v]) => [k, stringToRegex(v)]),
  );
}
