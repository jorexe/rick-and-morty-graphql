import fetch from 'node-fetch';

import Character from '../models/character.js';
import CharacterRepository from '../repositories/characterRepository.js';
import { clearDb } from '../utils/db.js';

// Flag that toggles fetching all the characters from r&m API
// true if we want to fetch all the characters
// false if we want to fetch only the first page
const RECURSIVE = true;

const API_URL = 'https://rickandmortyapi.com/api/character';

type CharacterApiResponse = {
  info: {
    count: number;
    pages: number;
    next: string;
    prev: string;
  };
  results: Character[];
};

function fetchCharacters(url: string): Promise<void> {
  return fetch(url)
    .then(r => {
      if (!r.ok) {
        throw new Error(r.statusText);
      }
      return r.json() as Promise<CharacterApiResponse>;
    })
    .then(r => {
      CharacterRepository.addCharacters(r.results);
      if (RECURSIVE && r.info.next) {
        return fetchCharacters(r.info.next);
      }
    });
}

await clearDb();
await fetchCharacters(API_URL);
