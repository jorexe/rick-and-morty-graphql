import Character from '../models/character.js';
import db from '../utils/db.js';

const PAGE_SIZE = 10;

function addCharacters(characters: Character[]): Promise<Character[]> {
  return db.insertAsync(characters);
}

async function findAll(filter: Partial<Character>, page: number) {
  const results = await db
    .find<Character[]>(filter)
    .skip(PAGE_SIZE * (page - 1))
    .limit(PAGE_SIZE);
  const count = await db.countAsync(filter);
  const pages = Math.ceil(count / PAGE_SIZE);

  return { results, count, pages };
}

const CharacterRepository = { addCharacters, findAll };

export default CharacterRepository;
