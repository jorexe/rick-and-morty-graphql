const typeDefs = `#graphql
  type Origin {
    name: String
    url: String
  }

  type Location {
    name: String
    url: String
  }

  type Character {
    id: Int!
    name: String!
    status: String
    species: String
    type: String
    gender: String
    origin: Origin
    location: Location
    image: String
    episode: [String]
    url: String
    created: String
  }

  input FilterCharacter {
    name: String
    status: String
    species: String
    type: String
    gender: String
  }

  type Pagination {
    count: Int!
    pages: Int!
    next: Int
    prev: Int
  }

  type PaginatedCharacters {
    info: Pagination
    results: [Character]
  }

  type Query {
    characters(page: Int, filter: FilterCharacter): PaginatedCharacters
  }
`;

export default typeDefs;
