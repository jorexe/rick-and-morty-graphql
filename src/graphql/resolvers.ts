import CharacterRepository from '../repositories/characterRepository.js';
import queryToRegex from '../utils/queryToRegex.js';

const resolvers = {
  Query: {
    characters: async (_: any, { page = 1, filter = {} }: any) => {
      const { results, count, pages } = await CharacterRepository.findAll(
        queryToRegex(filter),
        page,
      );

      return {
        results,
        info: {
          count,
          pages,
          prev: page > 1 ? page - 1 : null,
          next: page < pages ? page + 1 : null,
        },
      };
    },
  },
};

export default resolvers;
